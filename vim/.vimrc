let mapleader = ","
let g:mapleader = ","

execute pathogen#infect()
syntax on
filetype plugin indent on

"color settings
set background=dark
colorscheme molokai
"set terminal colors to 256
set t_Co=256

set number
set cursorline
set ts=2 sw=2 sts=2 expandtab
set hls

"swapfiles - move them to a sane location
set backupdir=~/.vim/backup//
set directory=~/.vim/swap//
set undodir=~/.vim/undo//

"folding
set foldmethod=syntax
set foldlevelstart=1
let javaScript_fold=1
let ruby_fold=1
let sh_fold_enabled=1
let vimsyn_folding=1
let xml_syntax_folding=1
let php_folding=1

"splits
set splitbelow
set splitright

"WhiteSpace matching
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

" Help keep lines readable in github diffs.
"set colorcolumn 120

"NerdTree
nmap <leader>n :NERDTreeToggle<cr>
autocmd vimenter * if !argc() | NERDTree | endif


