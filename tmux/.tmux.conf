#remap to screen prefix key
set -g prefix C-a
bind C-a send-prefix
unbind C-b

#speed up responsiveness, so vim is snappier
set -s escape-time 1

#window/pane management
set -g base-index 1
set -g pane-base-index 1

bind | split-window -h
bind - split-window -v

bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R
bind -r C-h select-window -t :-
bind -r C-l select-window -t :+

#resizing
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5

#reload configuration
bind r source-file ~/.tmux.conf \; display "Reloaded!"

#colour scheme
set -g default-terminal "screen-256color"

#mouse handling
setw -g mode-mouse on
set -g mouse-select-pane on
set -g mouse-resize-pane on
set -g mouse-select-window onu

#make it more vi like!
setw -g mode-keys vi

#copying like a boss
set-option -g default-command "/opt/local/bin/reattach-to-user-namespace /opt/local/bin/zsh"
bind C-c run "tmux save-buffer - | reattach-to-user-namespace pbcopy"
bind C-v run "tmux set-buffer \"$(reattach-to-user-namespace pbpaste)\"; tmux paste-buffer"
