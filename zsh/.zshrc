export SVN_EDITOR="vim --noplugin"
export EDITOR="vim --noplugin"

export PATH=/opt/local/bin:/opt/local/sbin:/opt/local/include/:/opt/local/lib/postgresql84/bin:/opt/local/apache2/bin:~/tools/:$PATH

# Path to your oh-my-zsh configuration.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
export ZSH_THEME="gentoo"
#export ZSH_THEME="robbyrussell"

# Set to this to use case-sensitive completion
export CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# export DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# export DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# export DISABLE_AUTO_TITLE="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git mercurial composer gem macports textmate ruby rails taskwarrior rvm svn symfony symfony2 rake rspec bundler apache2-macports postgres osx)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
export PATH
[[ -s "/Users/sarcas/.rvm/scripts/rvm" ]] && source "/Users/sarcas/.rvm/scripts/rvm"  # This loads RVM into a shell session.

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
alias rake='noglob rake'
